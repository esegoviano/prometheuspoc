# spring-boot 2 + prometheus + grafana poc

This is a sample [spring-boot-2](https://spring.io/projects/spring-boot) app  and a monitoring solution using [Prometheus](https://prometheus.io/) and [Grafana](https://grafana.com/).

## Built With

* App
  * [Springboot 2.2.6](https://spring.io/projects/spring-boot)
  * Spring Web for the REST api
  * [H2 Database] (https://www.h2database.com/html/main.html)
  * Spring Data JPA
  * [modelmapper](http://modelmapper.org/) to map entities & dto
  * [Lombok](https://projectlombok.org/)
  * [Swagger](https://swagger.io/)
* Testing
  * [Junit 5](https://junit.org/junit5/)
  * [Mockito] (https://site.mockito.org/)
  * MockMvc
* Metrics
  * Spring boot actuator
  * [Micrometer](https://micrometer.io/)
  * [Prometheus](https://prometheus.io/) 
  * [Grafana](https://grafana.com/)

## How to run

Requirements to run the application
* Docker, Docker Compose

Run application:

```bash
$ git clone https://github.com/aha-oretama/prometheuspoc.git
$ cd prometheuspoc
$ docker-compose up -d
``` 

Three applications are going to start.

| Application | URL |
|-------------|------|
|spring boot web sample app| http://localhost:8888 |
|spring boot web sample app actuator| http://localhost:8888/prometheus/actuator |
|Prometheus | http://localhost:9090 |
|Grafana | http://localhost:3000 |

The http://localhost:8888 redirects to Swagger UI to consume the API easily

## Check Prometheus

In order to check that Prometheus is scrapping our app you have to go to targets tab

* http://localhost:9090/targets

![Alt text](images/prometheus.png "Prometheus Target")

## See metrics on Grafana

You have to add the prometheus datasource
![Alt text](images/grafana_prometheus_ds.png "Prometheus 
ds")

Then you can add the metrics you need one or more dashboards

Since the community has developed a really good ones, I have used [this one](https://grafana.com/grafana/dashboards/9845)
It is also reposited on grafanaDashboards folder. You have to import it and see the metrics

### BASIC STATS
![Alt text](images/grafana_CPU.png "Basic Stats")
### HIKARI DB POOL
![Alt text](images/grafana_HIKARI.png "Hikari pool")
### HTTP STATS
![Alt text](images/grafana_HTTP_STATS.png "Http Stats")
package org.eder.prometheuspoc.web;

import org.eder.prometheuspoc.domain.dto.UserDTO;
import org.eder.prometheuspoc.domain.entity.enums.UserType;
import org.eder.prometheuspoc.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.UUID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    private static final String NAME = "Eder";
    private static final String SURE_NAME = "Segoviano";
    private static final LocalDate DATE_OF_BIRTH = LocalDate.of(1982, 12, 9);
    private static final String EMAIL = "eder@mail.com";
    private static final UserType USER_TYPE = UserType.PREMIUM;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    @Test
    void getUser() throws Exception {
        UserDTO savedDto = userService.save(getUserDto());
        this.mockMvc.perform(get("/user/"+ savedDto.getUserId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId").value(savedDto.getUserId().toString()))
                .andExpect(jsonPath("$.name").value(savedDto.getName()))
                .andExpect(jsonPath("$.sureName").value(savedDto.getSureName()))
                .andExpect(jsonPath("$.dateOfBirth").value(savedDto.getDateOfBirth().toString()))
                .andExpect(jsonPath("$.email").value(savedDto.getEmail()))
                .andExpect(jsonPath("$.userType").value(savedDto.getUserType().toString()));
    }

    @Test
    void getUserNotFound() throws Exception {
        this.mockMvc.perform(get("/user/"+ UUID.randomUUID()))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    //TODO: IMPLEMENT MISSED TESTS

    private UserDTO getUserDto() {
        return UserDTO.builder()
                .name(NAME)
                .sureName(SURE_NAME)
                .dateOfBirth(DATE_OF_BIRTH)
                .email(EMAIL)
                .userType(USER_TYPE)
                .build();
    }
}
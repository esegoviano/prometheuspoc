package org.eder.prometheuspoc.service.impl;

import org.eder.prometheuspoc.domain.dto.UserDTO;
import org.eder.prometheuspoc.exceptions.UserNotFoundException;
import org.eder.prometheuspoc.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import java.util.UUID;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class UserServiceImplIT {

    private static final UUID USER_ID = UUID.randomUUID();

    @Autowired
    UserService userService;

    @DisplayName("Get User Success Test")
    @Test
    void getUserSuccess() throws UserNotFoundException {
        UserDTO savedUser = userService.save(getRandomUserDto());
        assertNotNull(userService.getUser(savedUser.getUserId()));
    }

    @DisplayName("Get User Not Found Test")
    @Test
    void getUserNotFound() {
        assertThrows(UserNotFoundException.class,
                () -> userService.getUser(USER_ID));
    }

    @DisplayName("Get All Users Test")
    @Test
    void getAllUsers() {
        userService.save(getRandomUserDto());
        userService.save(getRandomUserDto());
        userService.save(getRandomUserDto());

        assertEquals(userService.getAllUsers().size(), 3);
    }

    @DisplayName("Remove user Test")
    @Test
    void removeUser() throws UserNotFoundException {
        UserDTO savedUser = userService.save(getRandomUserDto());
        assertTrue(userService.removeUser(savedUser.getUserId()));
    }

    @DisplayName("User Not found on remove user Test")
    @Test
    void userNotFoundOnRemoveUser() {
        assertThrows(UserNotFoundException.class,
                () -> userService.removeUser(UUID.randomUUID()));
    }

    @DisplayName("Save an user Test")
    @Test
    void save() {
        assertNotNull(userService.save(getRandomUserDto()));
    }

    private UserDTO getRandomUserDto() {
        return UserDTO.builder()
                .userId(UUID.randomUUID())
                .build();
    }
}
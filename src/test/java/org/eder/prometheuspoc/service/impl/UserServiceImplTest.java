package org.eder.prometheuspoc.service.impl;

import org.eder.prometheuspoc.domain.dto.UserDTO;
import org.eder.prometheuspoc.domain.entity.UserEntity;
import org.eder.prometheuspoc.domain.entity.enums.UserType;
import org.eder.prometheuspoc.domain.mappers.user.UserMapperImpl;
import org.eder.prometheuspoc.exceptions.UserNotFoundException;
import org.eder.prometheuspoc.repository.UserRepository;
import org.eder.prometheuspoc.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

class UserServiceImplTest {

    private static final UUID USER_ID = UUID.randomUUID();
    private static final String NAME = "Eder";
    private static final String SURE_NAME = "Segoviano";
    private static final LocalDate DATE_OF_BIRTH = LocalDate.of(1982, 12, 9);
    private static final String EMAIL = "eder@mail.com";
    private static final UserType USER_TYPE = UserType.PREMIUM;


    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserService userService = new UserServiceImpl(userRepository, new UserMapperImpl());

    @BeforeEach
    void init_mocks() {
        MockitoAnnotations.initMocks(this);
    }

    @DisplayName("Get User Success Test")
    @Test
    void getUserSuccess() throws UserNotFoundException {
        when(userRepository.getByUserId(USER_ID)).thenReturn(Optional.of(getUserEntity()));

        UserDTO user = userService.getUser(USER_ID);
        assertAll("userDtoFields",
                () -> assertEquals(user.getUserId(), USER_ID),
                () -> assertEquals(user.getName(), NAME),
                () -> assertEquals(user.getSureName(), SURE_NAME),
                () -> assertEquals(user.getEmail(), EMAIL),
                () -> assertEquals(user.getDateOfBirth(), DATE_OF_BIRTH),
                () -> assertEquals(user.getUserType(), USER_TYPE)
        );

    }

    @DisplayName("Get User Not Found Test")
    @Test
    void getUserNotFound() {
        when(userRepository.getByUserId(USER_ID)).thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class,
                () -> userService.getUser(USER_ID));
    }

    @DisplayName("Get All Users Test")
    @Test
    void getAllUsers() {
        when(userRepository.findAll()).thenReturn(Arrays.asList(getUserEntity(),getUserEntity()));
        assertEquals(userService.getAllUsers().size(), 2);
    }

    @DisplayName("Remove user Test")
    @Test
    void removeUser() throws UserNotFoundException {
        assertTrue(userService.removeUser(USER_ID));
    }

    @DisplayName("User Not found on remove user Test")
    @Test
    void userNotFoundOnRemoveUser() {
        doThrow(EmptyResultDataAccessException.class).when(userRepository).deleteById(USER_ID);
        assertThrows(UserNotFoundException.class,
                () -> userService.removeUser(USER_ID));
    }

    @DisplayName("Save an user Test")
    @Test
    void save() {
        UserEntity userEntity = getUserEntity();
        when(userRepository.save(userEntity)).thenReturn(userEntity);
        UserDTO savedUserDTO = userService.save(getUserDto());
        assertAll("userDtoFields",
                () -> assertEquals(savedUserDTO.getUserId(), USER_ID),
                () -> assertEquals(savedUserDTO.getName(), NAME),
                () -> assertEquals(savedUserDTO.getSureName(), SURE_NAME),
                () -> assertEquals(savedUserDTO.getEmail(), EMAIL),
                () -> assertEquals(savedUserDTO.getDateOfBirth(), DATE_OF_BIRTH),
                () -> assertEquals(savedUserDTO.getUserType(), USER_TYPE)
        );
    }

    private UserEntity getUserEntity() {
        return UserEntity.builder()
                .userId(USER_ID)
                .name(NAME)
                .sureName(SURE_NAME)
                .dateOfBirth(DATE_OF_BIRTH)
                .email(EMAIL)
                .userType(USER_TYPE)
                .build();
    }

    private UserDTO getUserDto() {
        return UserDTO.builder()
                .userId(USER_ID)
                .name(NAME)
                .sureName(SURE_NAME)
                .dateOfBirth(DATE_OF_BIRTH)
                .email(EMAIL)
                .userType(USER_TYPE)
                .build();
    }
}
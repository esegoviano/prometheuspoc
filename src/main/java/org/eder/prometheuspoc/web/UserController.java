package org.eder.prometheuspoc.web;


import lombok.AllArgsConstructor;
import org.eder.prometheuspoc.domain.dto.UserDTO;
import org.eder.prometheuspoc.exceptions.UserNotFoundException;
import org.eder.prometheuspoc.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    UserService userService;

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<UserDTO> handleUserNotFoundException() {
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }



    @GetMapping
    public List<UserDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping(value = "/{userId}")
    public ResponseEntity<UserDTO> getUser(@PathVariable("userId") UUID userId) throws UserNotFoundException {
         return ResponseEntity.ok(userService.getUser(userId));
    }


    @DeleteMapping(value = "/{userId}")
    public ResponseEntity<String> deleteUser(@PathVariable("userId") UUID userId) throws UserNotFoundException {
        userService.removeUser(userId);
        return ResponseEntity.ok("User with id : " + userId + " removed");
    }

    @PostMapping
    public ResponseEntity<UserDTO> addNewUser(@Valid @RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(userService.save(userDTO));
    }



}

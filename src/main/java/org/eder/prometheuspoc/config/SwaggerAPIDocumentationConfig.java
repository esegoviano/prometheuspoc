package org.eder.prometheuspoc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;

@EnableSwagger2
@Configuration
public class SwaggerAPIDocumentationConfig {
    ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("User REST CRUD operations API in Spring-Boot 2")
                .description(
                        "Sample REST API for monitoring using Spring Boot, Prometheus and Grafana")
                .build();
    }

    @Bean
    public Docket configureControllerPackageAndConvertors() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("org.eder.prometheuspoc")).build()
                .directModelSubstitute(LocalDate.class, String.class)
                .apiInfo(apiInfo());
    }
}
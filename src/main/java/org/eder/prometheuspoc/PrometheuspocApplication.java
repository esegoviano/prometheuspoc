package org.eder.prometheuspoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrometheuspocApplication {

  public static void main(String[] args) {
    SpringApplication.run(PrometheuspocApplication.class, args);
  }

}

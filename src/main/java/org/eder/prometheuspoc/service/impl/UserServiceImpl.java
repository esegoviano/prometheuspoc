package org.eder.prometheuspoc.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eder.prometheuspoc.domain.dto.UserDTO;
import org.eder.prometheuspoc.domain.entity.UserEntity;
import org.eder.prometheuspoc.domain.mappers.user.UserMapper;
import org.eder.prometheuspoc.exceptions.UserNotFoundException;
import org.eder.prometheuspoc.repository.UserRepository;
import org.eder.prometheuspoc.service.UserService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    UserRepository repository;
    UserMapper userMapper;

    @Override
    public UserDTO getUser(UUID userId) throws UserNotFoundException {

        return repository.getByUserId(userId)
                .map(e -> userMapper.mapToDto(e))
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<UserDTO> getAllUsers() {
        List<UserEntity> userEntities = repository.findAll();
        log.debug("Get users from db: {}", userEntities);
        return userMapper.mapListToDto(userEntities);
    }


    @Override
    public boolean removeUser(UUID userId) throws UserNotFoundException {
        try {
            repository.deleteById(userId);
            log.debug("User deleted from db: {}", userId);
        } catch (EmptyResultDataAccessException e){
            throw new UserNotFoundException();
        }

        return true;
    }

    @Override
    public UserDTO save(UserDTO userDTO) {
        UserEntity savedUser = repository.save(userMapper.mapToEntity(userDTO));
        log.debug("User saved on db: {}", savedUser);
        return userMapper.mapToDto(savedUser);
    }
}

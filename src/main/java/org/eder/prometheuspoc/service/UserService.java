package org.eder.prometheuspoc.service;

import org.eder.prometheuspoc.domain.dto.UserDTO;
import org.eder.prometheuspoc.exceptions.UserNotFoundException;

import java.util.List;
import java.util.UUID;

public interface UserService {
    UserDTO getUser(UUID userId) throws UserNotFoundException;
    List<UserDTO> getAllUsers();
    boolean removeUser(UUID userId) throws UserNotFoundException;
    UserDTO save(UserDTO userDTO);
}

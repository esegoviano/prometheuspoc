package org.eder.prometheuspoc.repository;

import org.eder.prometheuspoc.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    Optional<UserEntity> getByUserId(UUID userId);
}

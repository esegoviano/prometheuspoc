package org.eder.prometheuspoc.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.eder.prometheuspoc.domain.entity.enums.UserType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "User", description = "A user ")
public class UserDTO extends AbstractDTO {
    @ApiModelProperty(value = "The unique identifier of the user", readOnly = true, example = "04f4b5c2-7b3c-4a91-83e0-677e98693811")
    private UUID userId;
    @ApiModelProperty(value = "Name of the user", required = true, example = "Thelonious")
    @NotNull(message = "NotNull.userDTO.name")
    @Size(min = 1, max = 255, message = "Size.userDTO.name")
    private String name;
    @ApiModelProperty(example = "Monk")
    private String sureName;
    @ApiModelProperty(value = "Email of the user", required = true, example = "thelonious@gmail.com")
    @NotNull(message = "NotNull.userDTO.email")
    private String email;
    @ApiModelProperty(example = "1999-05-05")
    private LocalDate dateOfBirth;
    @ApiModelProperty(value = "User type", required = true, example = "REGULAR")
    @NotNull(message = "NotNull.userDTO.userType")
    private UserType userType;
}

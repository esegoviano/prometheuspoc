package org.eder.prometheuspoc.domain.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.eder.prometheuspoc.domain.entity.enums.UserType;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
@Table(name = "USER")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private UUID userId;
    private String name;
    private String sureName;
    private String email;
    private LocalDate dateOfBirth;
    private UserType userType;

}

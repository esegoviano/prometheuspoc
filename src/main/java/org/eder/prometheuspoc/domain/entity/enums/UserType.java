package org.eder.prometheuspoc.domain.entity.enums;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum UserType {
    REGULAR,
    PREMIUM,
    ADMIN
}

package org.eder.prometheuspoc.domain.mappers.user;

import lombok.AllArgsConstructor;
import org.eder.prometheuspoc.domain.dto.UserDTO;
import org.eder.prometheuspoc.domain.entity.UserEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapperImpl implements UserMapper {

    ModelMapper modelMapper;

    public UserMapperImpl(){
        modelMapper = new ModelMapper();
    }

    @Override
    public UserEntity mapToEntity(UserDTO dto) {
        return modelMapper.map(dto, UserEntity.class);
    }

    @Override
    public UserDTO mapToDto(UserEntity entity) {
        return modelMapper.map(entity, UserDTO.class);
    }
}

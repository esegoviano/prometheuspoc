package org.eder.prometheuspoc.domain.mappers;



import org.eder.prometheuspoc.domain.dto.AbstractDTO;
import org.eder.prometheuspoc.domain.entity.BaseEntity;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface GenericMapper<D extends AbstractDTO, E extends BaseEntity> {
    public E mapToEntity(D dto);
    public D mapToDto(E entity);
    default List<D> mapListToDto(final Collection<E> entities) {
        return entities.stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }

    default List<E> mapListToEntity(final Collection<D> dtos) {
        return dtos.stream()
                .map(this::mapToEntity)
                .collect(Collectors.toList());
    }
}
package org.eder.prometheuspoc.domain.mappers.user;

import org.eder.prometheuspoc.domain.dto.UserDTO;
import org.eder.prometheuspoc.domain.entity.UserEntity;
import org.eder.prometheuspoc.domain.mappers.GenericMapper;

public interface UserMapper extends GenericMapper<UserDTO, UserEntity> {
}
